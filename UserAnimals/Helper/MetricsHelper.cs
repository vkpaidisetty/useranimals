﻿using System;
using UserAnimals.Enums;

namespace UserAnimals.Helper
{
    internal static class MetricsHelper
    {
        public static Petting IsAnimalHappy(DateTime lastPetted, double pettingFrequency)
        {
            if ((lastPetted.AddSeconds(pettingFrequency) > DateTime.Now.AddSeconds(pettingFrequency / 3)) &&
                (lastPetted.AddSeconds(pettingFrequency) < DateTime.Now.AddSeconds(2 * (pettingFrequency / 3))))
            {
                return Petting.Neutral;
            }
            else if (lastPetted.AddSeconds(pettingFrequency) > DateTime.Now.AddSeconds(2 * (pettingFrequency / 3)))
            {
                return Petting.Happy;
            }
            else
            {
                return Petting.Sad;
            }
        }

        internal static Feeding IsAnimalFull(DateTime lastFed, double feedingFrequency)
        {
            if ((lastFed.AddSeconds(feedingFrequency) > DateTime.Now.AddSeconds(feedingFrequency / 3)) &&
               (lastFed.AddSeconds(feedingFrequency) < DateTime.Now.AddSeconds(2 * (feedingFrequency / 3))))
            {
                return Feeding.Neutral;
            }
            else if (lastFed.AddSeconds(feedingFrequency) > DateTime.Now.AddSeconds(2 * (feedingFrequency / 3)))
            {
                return Feeding.Full;
            }
            else
            {
                return Feeding.Hungry;
            }
        }
    }
}