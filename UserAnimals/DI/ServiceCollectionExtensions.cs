﻿using Microsoft.Extensions.DependencyInjection;
using OBV.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UserAnimals.DAL;
using UserAnimals.Factory;
using UserAnimals.Interfaces;
using UserAnimals.Interfaces.Repository;
using UserAnimals.Models;

namespace UserAnimals.DI
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection RegisterServices(this IServiceCollection services)
        {
            services.AddTransient<IAnimalRepository, AnimalRepository>();
            services.AddTransient<IAnimalFactory, AnimalFactory>();
            services.AddTransient<IConfigurationHelper, ConfigurationHelper>();
            services.AddTransient<IConnectionHelper, ConnectionHelper>();

            return services;
        }


        public static IServiceCollection AddControllersAsServices(this IServiceCollection services, IEnumerable<Type> controllerTypes)
        {
            foreach (var type in controllerTypes)
            {
                services.AddTransient(type);
            }

            return services;
        }
    }
}