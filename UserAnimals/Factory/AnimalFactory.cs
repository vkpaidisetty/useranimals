﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using UserAnimals.Interfaces;
using UserAnimals.Interfaces.Repository;
using UserAnimals.Models;

namespace UserAnimals.Factory
{
    public class AnimalFactory : IAnimalFactory
    {
        IAnimalRepository _animalRepository;

        public AnimalFactory(IAnimalRepository animalRepository)
        {
            _animalRepository = animalRepository;
        }
        public async Task<AnimalModel> FeedAnimalByID(int animalID)
        {
            return await _animalRepository.FeedAnimalByID(animalID);
        }

        public async Task<AnimalModel> GetAnimalByID(int animalID)
        {
            return await _animalRepository.GetAnimalByID(animalID);
        }

        public async Task<IList<AnimalModel>> GetAnimalList()
        {
            return await _animalRepository.GetAnimalList();
        }

        public async Task<AnimalModel> PetAnimalByID(int animalID)
        {
            return await _animalRepository.PetAnimalByID(animalID);
        }
    }
}