﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using UserAnimals.Enums;
using UserAnimals.Helper;

namespace UserAnimals.Models
{
    public class AnimalModel
    {
        public int AnimalID { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public AnimalType Type { get; set; }
        public int PettingFrequency { get; set; }
        public int FeedingFrequency { get; set; }
        public int UserID { get; set; }
        public DateTime LastFed { get; set; }
        public DateTime LastPetted { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public Petting IsHappy { get { return MetricsHelper.IsAnimalHappy(LastPetted, PettingFrequency); } }
        [JsonConverter(typeof(StringEnumConverter))]
        public Feeding IsHungry { get { return MetricsHelper.IsAnimalFull(LastFed, FeedingFrequency); } }

    }
}