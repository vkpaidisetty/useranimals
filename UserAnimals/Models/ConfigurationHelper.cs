﻿using OBV.Domain.Interfaces;
using System.Configuration;
using UserAnimals.DAL;
using UserAnimals.Interfaces;

namespace UserAnimals.Models
{
    public class ConfigurationHelper: IConfigurationHelper
    {
        public IConnectionFactory GetConnection()
        {
            ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings["UserAnimals"];

            return new DbConnectionFactory(settings.ConnectionString, settings.Name);
        }
    }
}