﻿using System.Collections.Generic;

namespace UserAnimals.Models
{
    public class UserModel
    {
        public int UserID { get; set; }
        public string Name { get; set; }
        public IList<AnimalModel> AnimalList { get; set; }
    }
}