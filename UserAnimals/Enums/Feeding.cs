﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace UserAnimals.Enums
{
    public enum Feeding
    {
        [Description("Hungry")]
        [EnumMember(Value = "Hungry")]
        Hungry = -1,
        [Description("Neutral")]
        [EnumMember(Value = "Neutral")]
        Neutral = 0,
        [Description("Full")]
        [EnumMember(Value = "Full")]
        Full = 1
    }
}