﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace UserAnimals.Enums
{
    public enum AnimalType
    {
        [Description("Cat")]
        [EnumMember(Value = "Cat")]
        Cat,
        [Description("Dog")]
        [EnumMember(Value = "Dog")]
        Dog,
        [Description("Bird")]
        [EnumMember(Value = "Bird")]
        Bird,
        [Description("Ferret")]
        [EnumMember(Value = "Ferret")]
        Ferret,
        [Description("Guinea_Pig")]
        [EnumMember(Value = "Guinea_Pig")]
        Guinea_Pig,
        [Description("Rabbit")]
        [EnumMember(Value = "Rabbit")]
        Rabbit,
         [Description("Hamster")]
        [EnumMember(Value = "Hamster")]
        Hamster

    }
}