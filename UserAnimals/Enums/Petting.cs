﻿namespace UserAnimals.Enums
{
    public enum Petting
    {
        Sad = -1,
        Neutral = 0,
        Happy=1
    }
}