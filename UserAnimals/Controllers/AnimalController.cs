﻿using Swashbuckle.Examples;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using UserAnimals.Enums;
using UserAnimals.Interfaces;
using UserAnimals.Models;
using UserAnimals.SwaggerExamples;

namespace UserAnimals.Controllers
{
    public class AnimalController : ApiController
    {
               
        private IAnimalFactory _animalFactory;
        public AnimalController(IAnimalFactory animalFactory)
        {
            _animalFactory = animalFactory;
        }
        // GET: api/Animal
        /// <summary>
        /// Get the list of Animals
        /// </summary>
        /// <returns></returns>
        /// <remarks>
        /// Returns a full list of animals
        /// </remarks>
        ///  [SwaggerOperation(Tags = new[] { "Animal" })]
        [SwaggerRequestExample(typeof(AnimalModel), typeof(AnimalsListModelExample))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(AnimalModel), Description = "Animals found and returned successfully")]
        [SwaggerResponseExample(HttpStatusCode.OK, typeof(AnimalsListModelExample))]
        [HttpGet]
        [Route("api/Animal")]
        public async Task<IHttpActionResult> Get()
        {
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, await _animalFactory.GetAnimalList()));            
        }

        // GET: api/Animal/5
        /// <summary>
        /// Get animal details by ID
        /// </summary>
        /// <param name="id">Animal ID</param>
        /// <remarks>
        /// Returns animal detail by ID
        /// </remarks>
        [SwaggerOperation(Tags = new[] { "Animal" })]
        [SwaggerRequestExample(typeof(AnimalModel), typeof(AnimalModelExample))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(List<AnimalModel>), Description = "Animal found and returned successfully")]
        [SwaggerResponseExample(HttpStatusCode.OK, typeof(AnimalModelExample))]
        [HttpGet]
        [Route("api/Animal/{id}")]
        public async Task<IHttpActionResult> Get(int id)
        {
            HttpResponseMessage responseMessage;
            AnimalModel animalModel = await _animalFactory.GetAnimalByID(id);
            double pettingFrequency = animalModel.PettingFrequency;
            double feedingFrequency = animalModel.FeedingFrequency;
            
            responseMessage = Request.CreateResponse(HttpStatusCode.OK, animalModel);
            return ResponseMessage(responseMessage);
        }

        // PUT: api/Animal/5
        /// <summary>
        /// Pet animal by ID
        /// </summary>
        /// <param name="id">Animal ID</param>
        /// <remarks>
        /// Returns animal info which was petted
        /// </remarks>
        /// [SwaggerOperation(Tags = new[] { "Animal" })]
        [SwaggerRequestExample(typeof(AnimalModel), typeof(AnimalModelExample))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(AnimalModel), Description = "Animal petted and returned successfully")]
        [SwaggerResponseExample(HttpStatusCode.OK, typeof(AnimalModelExample))]
        [HttpPut]
        [Route("api/Animal/{id}/Pet")]
        public async Task<IHttpActionResult> Put(int id)
        {
            if (id<1) return ResponseMessage(Request.CreateResponse(HttpStatusCode.PartialContent, "Please send valid animalID"));

            HttpResponseMessage responseMessage = Request.CreateResponse(HttpStatusCode.OK, await _animalFactory.PetAnimalByID(id));

            return ResponseMessage(responseMessage);
        }
        /// <summary>
        /// Feed animal by ID
        /// </summary>
        /// <param name="id">Animal ID</param>
        /// <remarks>
        /// Returns animal info which was fed
        /// </remarks>
        /// [SwaggerOperation(Tags = new[] { "Animal" })]
        [SwaggerRequestExample(typeof(AnimalModel), typeof(AnimalModelExample))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(AnimalModel), Description = "Animal fed and returned successfully")]
        [SwaggerResponseExample(HttpStatusCode.OK, typeof(AnimalModelExample))]
        [HttpPut]
        [Route("api/Animal/{id}/Feed")]
        public async Task<IHttpActionResult> Feed(int id)
        {
            if (id < 1) return ResponseMessage(Request.CreateResponse(HttpStatusCode.PartialContent, "Please send valid animalID"));

            HttpResponseMessage responseMessage = Request.CreateResponse(HttpStatusCode.OK, await _animalFactory.FeedAnimalByID(id));

            return ResponseMessage(responseMessage);
        }

       
    }
}
