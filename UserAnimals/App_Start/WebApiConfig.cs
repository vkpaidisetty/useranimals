﻿using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Mvc;
using UserAnimals.DI;

namespace UserAnimals
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API routes
            config.MapHttpAttributeRoutes();

            // Web API configuration and services
            var mediaType = new MediaTypeHeaderValue("application/json");

            var formatter = new JsonMediaTypeFormatter();
            formatter.SupportedMediaTypes.Clear();
            formatter.SupportedMediaTypes.Add(mediaType);

            config.Formatters.Clear();
            config.Formatters.Add(formatter);   

            var services = new ServiceCollection();
            ConfigureServices(services);
            var resolver = new DefaultDependencyResolver(services.BuildServiceProvider());
            config.DependencyResolver = resolver;
        }

        public static void ConfigureServices(IServiceCollection services)
        {
            services.RegisterServices();
            services.AddControllersAsServices(typeof(WebApiConfig).Assembly.GetExportedTypes()
                .Where(t => !t.IsAbstract && !t.IsGenericTypeDefinition)
                .Where(t => typeof(IController).IsAssignableFrom(t)
                    || typeof(System.Web.Http.Controllers.IHttpController).IsAssignableFrom(t)));
            
        }
    }
}
