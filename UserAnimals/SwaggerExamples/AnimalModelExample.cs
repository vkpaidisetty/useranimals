﻿using Swashbuckle.Examples;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UserAnimals.Enums;
using UserAnimals.Models;

namespace UserAnimals.SwaggerExamples
{
    public class AnimalModelExample : IExamplesProvider
    {
        public object GetExamples()
        {
            return new AnimalModel()
            {
                AnimalID = 1,
                Type = AnimalType.Cat,
                PettingFrequency = 3600,
                FeedingFrequency = 7200,
                UserID = 1,
                LastFed = DateTime.Now,
                LastPetted = DateTime.Now
            };
        }
    }
}