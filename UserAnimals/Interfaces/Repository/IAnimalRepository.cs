﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UserAnimals.Models;

namespace UserAnimals.Interfaces.Repository
{
    public interface IAnimalRepository
    {
        Task<AnimalModel> GetAnimalByID(int id);
        Task<IList<AnimalModel>> GetAnimalList();
        Task<AnimalModel> FeedAnimalByID(int id);
        Task<AnimalModel> PetAnimalByID(int id);
    }
}
