﻿using System.Configuration;

namespace UserAnimals.Interfaces.Repository
{
    public interface IConnectionHelper
    {
        IConnectionFactory GetConfigConnection();       
    }
}
