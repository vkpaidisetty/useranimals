﻿using UserAnimals.Interfaces;

namespace OBV.Domain.Interfaces
{
    public interface IConfigurationHelper
    {
        IConnectionFactory GetConnection();
    }
}
