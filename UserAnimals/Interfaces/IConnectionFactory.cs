﻿using System.Data;

namespace UserAnimals.Interfaces
{
    public interface IConnectionFactory
    {
        IDbConnection Create();
    }
}
