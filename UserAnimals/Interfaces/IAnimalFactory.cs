﻿
using System.Collections.Generic;
using System.Threading.Tasks;
using UserAnimals.Models;

namespace UserAnimals.Interfaces
{
    public interface IAnimalFactory
    {
        Task<AnimalModel> GetAnimalByID(int animalID);
        Task<IList<AnimalModel>> GetAnimalList();
        Task<AnimalModel> PetAnimalByID(int animalID);
        Task<AnimalModel> FeedAnimalByID(int animalID);
    }
}
