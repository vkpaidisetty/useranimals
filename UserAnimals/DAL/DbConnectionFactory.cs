﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using UserAnimals.Interfaces;

namespace UserAnimals.DAL
{
    public class DbConnectionFactory: IConnectionFactory
    {
        private readonly string _connectionString;
        private readonly string _name;

        public DbConnectionFactory(string connectionString, string name)
        {
            if (string.IsNullOrEmpty(connectionString))
                throw new ConfigurationErrorsException(string.Format("Failed to find connection string named '{0}' in config.", name));

            _name = name;
            _connectionString = connectionString;
        }

        public IDbConnection Create()
        {
            var provider = new SqlConnection(_connectionString);
            if (provider == null)
                throw new ConfigurationErrorsException(string.Format("Failed to create a connection using the connection string named '{0}' in app/web.config.", _name));
            provider.Open();
            return provider;
        }
    }
}