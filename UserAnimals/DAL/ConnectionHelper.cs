﻿using OBV.Domain.Interfaces;
using System.Configuration;
using UserAnimals.Interfaces;
using UserAnimals.Interfaces.Repository;

namespace UserAnimals.DAL
{
    public class ConnectionHelper : IConnectionHelper
    {
        private IConfigurationHelper _configurationHelper;

        public ConnectionHelper( IConfigurationHelper configurationHelper) 
        {
            _configurationHelper = configurationHelper;
        }

        public IConnectionFactory GetConfigConnection()
        {
            return _configurationHelper.GetConnection();
        }
    }
}