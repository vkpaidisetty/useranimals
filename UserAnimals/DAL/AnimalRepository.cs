﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using UserAnimals.Enums;
using UserAnimals.Interfaces.Repository;
using UserAnimals.Models;

namespace UserAnimals.DAL
{
    public class AnimalRepository : IAnimalRepository
    {
        private IConnectionHelper _connectionHelper;

        public AnimalRepository(IConnectionHelper connectionHelper)
        {
            _connectionHelper = connectionHelper;
        }

        public async Task<AnimalModel> GetAnimalByID(int animalId)
        {
            AnimalModel result = null;
            try
            {
                using (var connection = _connectionHelper.GetConfigConnection().Create())
                {
                    using (var command = (SqlCommand)connection.CreateCommand())
                    {
                        command.Parameters.Add(new SqlParameter("@animalId", SqlDbType.Int) { Value = animalId });
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "spGetAnimalInfoByID";

                        using (DbDataReader reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                result = new AnimalModel()
                                {
                                    AnimalID = (int)reader["AnimalID"],
                                    Type = ParseEnum<AnimalType>(reader["Type"].ToString()),
                                    PettingFrequency = (int) reader["PettingFrequency"],
                                    FeedingFrequency = (int)reader["FeedingFrequency"],
                                    UserID = (int)reader["UserID"],
                                    LastFed = (DateTime)reader["LastFed"],
                                    LastPetted = (DateTime)reader["LastPetted"]

                                };
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);                
            }
            return result;
            
        }
       
        public async Task<IList<AnimalModel>> GetAnimalList()
        {
            IList<AnimalModel> result = null;
            try
            {
                using (var connection = _connectionHelper.GetConfigConnection().Create())
                {
                    using (var command = (SqlCommand)connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "spGetAnimalsList";

                        using (DbDataReader reader = await command.ExecuteReaderAsync())
                        {
                            result = new List<AnimalModel>();
                            while (await reader.ReadAsync())
                            {
                                result.Add(new AnimalModel()
                                {
                                    AnimalID = (int)reader["AnimalID"],
                                    Type = ParseEnum<AnimalType>(reader["Type"].ToString()),
                                    PettingFrequency = (int)reader["PettingFrequency"],
                                    FeedingFrequency = (int)reader["FeedingFrequency"],
                                    UserID = (int)reader["UserID"],
                                    LastFed = (DateTime)reader["LastFed"],
                                    LastPetted = (DateTime)reader["LastPetted"]

                                });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return result;
        }

        public async Task<AnimalModel> FeedAnimalByID(int animalId)
        {
            AnimalModel result = null;
            try
            {
                using (var connection = _connectionHelper.GetConfigConnection().Create())
                {
                    using (var command = (SqlCommand)connection.CreateCommand())
                    {
                        command.Parameters.Add(new SqlParameter("@animalId", SqlDbType.Int) { Value = animalId });
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "spFeedAnimalById";

                        using (DbDataReader reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                result = new AnimalModel()
                                {
                                    AnimalID = (int)reader["AnimalID"],
                                    Type = ParseEnum<AnimalType>(reader["Type"].ToString()),
                                    PettingFrequency = (int)reader["PettingFrequency"],
                                    FeedingFrequency = (int)reader["FeedingFrequency"],
                                    UserID = (int)reader["UserID"],
                                    LastFed = (DateTime)reader["LastFed"],
                                    LastPetted = (DateTime)reader["LastPetted"]

                                };
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return result;

        }

        public async Task<AnimalModel> PetAnimalByID(int animalId)
        {
            AnimalModel result = null;
            try
            {
                using (var connection = _connectionHelper.GetConfigConnection().Create())
                {
                    using (var command = (SqlCommand)connection.CreateCommand())
                    {
                        command.Parameters.Add(new SqlParameter("@animalId", SqlDbType.Int) { Value = animalId });
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "spPetAnimalById";

                        using (DbDataReader reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                result = new AnimalModel()
                                {
                                    AnimalID = (int)reader["AnimalID"],
                                    Type = ParseEnum<AnimalType>(reader["Type"].ToString()),
                                    PettingFrequency = (int)reader["PettingFrequency"],
                                    FeedingFrequency = (int)reader["FeedingFrequency"],
                                    UserID = (int)reader["UserID"],
                                    LastFed = (DateTime)reader["LastFed"],
                                    LastPetted = (DateTime)reader["LastPetted"]

                                };
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return result;
        }

        private static T ParseEnum<T>(string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }
    }
}